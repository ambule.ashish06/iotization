import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  name='';
  password = '';
  username = '';
  show = '';
  constructor() { }

  ngOnInit() {
  }
  onSubmit() {
    this.show = 'Username ' + this.username + ',name '+this.name +' and password ' + this.password;
  }
}

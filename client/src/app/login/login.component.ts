import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  password = '';
  username = '';
  show = '';
  constructor(public userService: UserService) { }

  ngOnInit() {
  };
  onSubmit() {
    this.show = 'Username ' + this.username + 'and password ' + this.password;
    const sendUser = {
      userName: this.username,
      password: this.password
    };

    this.userService.login(sendUser).subscribe(res => {
      console.log(res)
    });
  };
};

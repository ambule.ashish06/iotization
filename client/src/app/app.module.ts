import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DataComponent } from './data/data.component';
import { FormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { ControlsComponent } from './controls/controls.component';
import { UserService } from './user.service';
import { HttpModule } from '@angular/http';

const appRoutes: Routes = [
  //{ path: '/', component: AppComponent },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'controls', component: ControlsComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'data', component: DataComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    DataComponent,

    RegisterComponent,

    ControlsComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
    FormsModule,
    HttpModule

  ],


  providers: [UserService],

  bootstrap: [AppComponent]
})
export class AppModule { }
